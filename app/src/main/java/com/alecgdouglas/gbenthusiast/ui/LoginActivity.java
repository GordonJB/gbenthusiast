package com.alecgdouglas.gbenthusiast.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v17.leanback.app.GuidedStepFragment;
import android.support.v17.leanback.widget.GuidanceStylist;
import android.support.v17.leanback.widget.GuidedAction;
import android.util.Log;

import com.alecgdouglas.gbenthusiast.ApiKeyManager;
import com.alecgdouglas.gbenthusiast.ApiKeyQuery;
import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.Utils;

import java.util.List;

public class LoginActivity extends CommonActivity {
    private static final String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            if (ApiKeyManager.hasApiKey(this)) {
                Log.d(TAG, "User has already saved an API key, finishing LoginActivity");
                finishAfterTransition();
            } else {
                Log.d(TAG, "User has not yet saved an API key");
                GuidedStepFragment
                        .addAsRoot(this, new EnterLinkCodeStepFragment(), android.R.id.content);
            }
        }
    }

    private enum LoginAction {
        CANCEL(R.string.guidedstep_cancel),
        ENTER_LINK_CODE(R.string.guidedstep_enter_link_code),
        NEVER_MIND(R.string.guidedstep_nevermind),
        RE_ENTER_CODE(R.string.guidedstep_re_enter_code);

        private final int mStringId;

        LoginAction(int stringId) {
            mStringId = stringId;
        }

        public int getId() {
            return ordinal();
        }

        public GuidedAction getAction(Context context) {
            return getAction(context, false, false);
        }

        public GuidedAction getAction(Context context, boolean editable,
                                      boolean descriptionEditable) {
            return new GuidedAction.Builder(context)
                    .title(context.getResources().getString(mStringId)).editable(editable)
                    .descriptionEditable(descriptionEditable).id(getId()).build();
        }
    }

    public static class EnterLinkCodeStepFragment extends GuidedStepFragment {
        private SpinnerFragment mSpinnerFragment;
        private ApiKeyQuery mApiKeyQuery;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mSpinnerFragment = new SpinnerFragment();
        }

        @Override
        public void onStart() {
            super.onStart();
            // When returning from failing to authenticate to re-enter the code, the selected step
            // may be > 0, i.e. not on the "Enter link code" action. Reset it here to 0 if that's
            // the case.
            if (getSelectedActionPosition() > 0) {
                setSelectedActionPosition(0);
            }
        }

        @Override
        public void onStop() {
            if (mApiKeyQuery != null && mApiKeyQuery.isRunning()) {
                mApiKeyQuery.cancel();
            }
            super.onStop();
        }

        @Override
        @NonNull
        public GuidanceStylist.Guidance onCreateGuidance(@NonNull Bundle savedInstanceState) {
            String title = getString(R.string.guidedstep_enter_link_code_title);
            String description = getString(R.string.guidedstep_enter_link_code_description);
            String breadcrumb = getString(R.string.guidedstep_enter_link_code_breadcrumb);
            return new GuidanceStylist.Guidance(title, description, breadcrumb, null);
        }

        @Override
        public void onCreateActions(@NonNull List<GuidedAction> actions,
                                    Bundle savedInstanceState) {
            actions.add(LoginAction.ENTER_LINK_CODE.getAction(getActivity(), false, true));
            actions.add(LoginAction.NEVER_MIND.getAction(getActivity()));
        }

        @Override
        public void onGuidedActionClicked(GuidedAction action) {
            if (action.getId() == LoginAction.ENTER_LINK_CODE.getId()) {
                getFragmentManager().beginTransaction().add(android.R.id.content, mSpinnerFragment)
                        .commit();
                mApiKeyQuery = new ApiKeyQuery();
                mApiKeyQuery.run(getActivity(), action.getDescription().toString(),
                        new ApiKeyQuery.Callback() {
                            @Override
                            public void onQueryComplete(boolean success) {
                                if (success) {
                                    Utils.showToast(getActivity(),
                                            R.string.guidedstep_success_toast_message);
                                    getActivity().setResult(RESULT_OK);
                                    getActivity().finishAfterTransition();
                                } else {
                                    getFragmentManager().beginTransaction().remove(mSpinnerFragment)
                                            .commit();
                                    GuidedStepFragment
                                            .add(getFragmentManager(), new FailureStepFragment());
                                }
                            }
                        });
            } else {
                getActivity().finishAfterTransition();
            }
        }
    }

    public static class FailureStepFragment extends GuidedStepFragment {
        @Override
        @NonNull
        public GuidanceStylist.Guidance onCreateGuidance(@NonNull Bundle savedInstanceState) {
            String title = getString(R.string.guidedstep_failure_title);
            String description = getString(R.string.guidedstep_failure_description);
            return new GuidanceStylist.Guidance(title, description, null, null);
        }

        @Override
        public void onCreateActions(@NonNull List<GuidedAction> actions,
                                    Bundle savedInstanceState) {
            actions.add(LoginAction.RE_ENTER_CODE.getAction(getActivity()));
            actions.add(LoginAction.CANCEL.getAction(getActivity()));
        }

        @Override
        public void onGuidedActionClicked(GuidedAction action) {
            if (action.getId() == LoginAction.RE_ENTER_CODE.getId()) {
                getFragmentManager().popBackStack();
            } else {
                getActivity().finishAfterTransition();
            }
        }
    }
}
