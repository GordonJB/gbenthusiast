package com.alecgdouglas.gbenthusiast;

import android.os.Handler;
import android.util.Log;

import com.alecgdouglas.gbenthusiast.model.Video;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.TimerTask;

public class LiveCheckTask extends TimerTask {
    private static final String TAG = "LiveCheckTask";

    private static final String LIVE_VIDEO_API = "video/current-live";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_VIDEO_OBJECT = "video";
    private static final String TAG_TITLE = "title";
    private static final String TAG_IMAGE = "image";
    private static final String TAG_STREAM = "stream";
    private final Handler mHandler = new Handler();
    private final Callback mCallback;
    private final String mApiKey;
    private final String mDefaultTitle;

    public LiveCheckTask(Callback callback, String apiKey, String defaultTitle) {
        mCallback = callback;
        mApiKey = apiKey;
        mDefaultTitle = defaultTitle;
    }

    @Override
    public void run() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                performLiveCheck();
            }
        });
    }

    private void performLiveCheck() {
        Log.d(TAG, "Checking for live stream");
        final ApiGetRequestParams params = new ApiGetRequestParams(mApiKey, LIVE_VIDEO_API);
        final ApiGetRequestTask task = new ApiGetRequestTask() {
            @Override
            protected void onPostExecute(Map<ApiGetRequestParams, JSONObject> responses) {
                final JSONObject responseJson = responses.get(params);
                if (responseJson == null) {
                    mCallback.onCheckComplete(null);
                    return;
                }
                final int success = responseJson.optInt(TAG_SUCCESS);
                if (success <= 0) {
                    mCallback.onCheckComplete(null);
                    return;
                }

                if (!responseJson.isNull(TAG_VIDEO_OBJECT)) {
                    try {
                        mCallback.onCheckComplete(createVideoFromJsonObject(
                                responseJson.getJSONObject(TAG_VIDEO_OBJECT)));
                        return;
                    } catch (JSONException e) {
                        // Fall through to return null.
                        Log.d(TAG, "Unable to get video object from JSON response", e);
                    }
                } else {
                    Log.d(TAG, "Live stream JSON video object is null");
                }
                mCallback.onCheckComplete(null);
            }
        };
        task.execute(params);
    }

    private Video createVideoFromJsonObject(JSONObject videoJson) {
        final Video.Builder builder = new Video.Builder(mApiKey, -1);
        builder.setIsLive(true);
        try {
            if (!videoJson.isNull(TAG_STREAM)) {
                final String streamUrl = videoJson.getString(TAG_STREAM);
                if (streamUrl != null && !streamUrl.isEmpty()) {
                    builder.withLowUrl(streamUrl);
                    builder.withHighUrl(streamUrl);
                    builder.withHdUrl(streamUrl);
                } else {
                    Log.d(TAG, "Live stream URL is null or empty");
                    return null;
                }
            }

            if (!videoJson.isNull(TAG_TITLE)) {
                final String title = videoJson.getString(TAG_TITLE);
                if (title != null && !title.isEmpty()) {
                    builder.withName(title);
                } else {
                    builder.withName(mDefaultTitle);
                }
            }

            if (!videoJson.isNull(TAG_IMAGE)) {
                String imageUrl = videoJson.getString(TAG_IMAGE);
                if (imageUrl != null && !imageUrl.isEmpty()) {
                    // Live stream image URLs tend to leave the protocol off. Try to correct that
                    // here.
                    try {
                        URI imageUri = new URI(imageUrl);
                        if (imageUri.getScheme() == null) {
                            imageUrl = "https://" + imageUrl;
                        }
                        builder.withTinyImageUrl(imageUrl);
                        builder.withMediumImageUrl(imageUrl);
                        builder.withSuperImageUrl(imageUrl);
                        builder.withIconImageUrl(imageUrl);
                    } catch (URISyntaxException e) {
                        // Create the video without an image.
                    }
                }
            }
        } catch (JSONException e) {
            Log.d(TAG, "JSONException encountered during parsing of video JSON object", e);
            return null;
        }
        return builder.build();
    }

    public interface Callback {
        void onCheckComplete(Video liveVideo);
    }
}
