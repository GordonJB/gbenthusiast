package com.alecgdouglas.gbenthusiast.presenter;

import android.support.v17.leanback.widget.ImageCardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.ui.DrawableGridItem;

public class DrawableImageCardPresenter extends CardPresenter {
    public DrawableImageCardPresenter() {
        super(CardSize.SMALL);
    }

    @Override
    public void onBindImageCardView(ImageCardView cardView, Object item) {
        final DrawableGridItem gridItem = (DrawableGridItem) item;

        cardView.setTitleText(gridItem.getText());
        final ImageView mainImageView = cardView.getMainImageView();
        mainImageView.setImageResource(gridItem.getDrawableResourceId());
        mainImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        mainImageView.setImageAlpha(160);
        final int padding =
                cardView.getResources().getDimensionPixelSize(R.dimen.drawable_image_card_padding);
        mainImageView.setPadding(padding, padding, padding, padding);

        // Center the title and hide the content text if there is no content text.
        if (cardView.getContentText() == null || cardView.getContentText().toString().isEmpty()) {
            final TextView titleView = (TextView) cardView.getRootView()
                    .findViewById(android.support.v17.leanback.R.id.title_text);
            titleView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

            final View contentView = cardView.getRootView()
                    .findViewById(android.support.v17.leanback.R.id.content_text);
            if (contentView != null) {
                contentView.setVisibility(View.GONE);
            }
        }
    }
}
