package com.alecgdouglas.gbenthusiast.presenter;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v17.leanback.widget.ImageCardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alecgdouglas.gbenthusiast.R;
import com.alecgdouglas.gbenthusiast.model.Video;
import com.bumptech.glide.Glide;

public class VideoCardPresenter extends CardPresenter {
    private final String mProgressTextTag = "progress_text";
    private final String mDurationTextTag = "duration_text";

    public VideoCardPresenter() {
        super();
    }

    public VideoCardPresenter(CardSize cardSize) {
        super(cardSize);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        final ViewHolder viewHolder = super.onCreateViewHolder(parent);
        final ImageCardView cardView = (ImageCardView) viewHolder.view;
        final TextView contentText =
                (TextView) cardView.findViewById(android.support.v17.leanback.R.id.content_text);
        contentText.getLayoutParams().width = RelativeLayout.LayoutParams.WRAP_CONTENT;

        final RelativeLayout infoField = (RelativeLayout) cardView
                .findViewById(android.support.v17.leanback.R.id.info_field);

        final TextView progressText = new TextView(cardView.getContext(), null,
                android.support.v17.leanback.R.attr.imageCardViewContentStyle);
        progressText.setId(View.generateViewId());
        progressText.setTag(mProgressTextTag);

        final RelativeLayout.LayoutParams progressParams =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        progressParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        progressParams.addRule(RelativeLayout.ALIGN_TOP, contentText.getId());
        infoField.addView(progressText, progressParams);

        final TextView durationText = new TextView(cardView.getContext(), null,
                android.support.v17.leanback.R.attr.imageCardViewContentStyle);
        durationText.setId(View.generateViewId());
        durationText.setTag(mDurationTextTag);
        durationText.setPadding(durationText.getPaddingLeft(), durationText.getPaddingTop(),
                infoField.getPaddingRight(), durationText.getPaddingBottom());

        final RelativeLayout.LayoutParams durationParams =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        durationParams.addRule(RelativeLayout.LEFT_OF, progressText.getId());
        durationParams.addRule(RelativeLayout.ALIGN_TOP, contentText.getId());
        infoField.addView(durationText, durationParams);

        return viewHolder;
    }

    @Override
    public void onBindImageCardView(ImageCardView cardView, Object item) {
        final Video video = (Video) item;

        cardView.setTitleText(video.getName());
        cardView.setContentText(video.getFormattedPublishDate());

        if (video.getMediumImageUrl() != null) {
            Glide.with(cardView.getContext()).load(video.getMediumImageUrl()).centerCrop()
                    .error(R.drawable.error_thumbnail).into(cardView.getMainImageView());
        }

        final TextView progressText = (TextView) cardView.findViewWithTag(mProgressTextTag);
        if (!video.isLive()) {
            if (video.getProgress() == 1f) {
                final ColorMatrix desatMatrix = new ColorMatrix();
                desatMatrix.setSaturation(0);
                cardView.getMainImageView().setColorFilter(new ColorMatrixColorFilter(desatMatrix));
            } else {
                // If we don't reset the color matrix, then other videos may end up using it even if
                // their progress isn't 100%.
                cardView.getMainImageView().setColorFilter(null);
            }

            final TextView durationText = (TextView) cardView.findViewWithTag(mDurationTextTag);
            durationText.setText(video.getFormattedDuration());

            progressText.setText(video.getFormattedProgressPercent());
        } else {
            progressText.setText(R.string.live_progress);
        }
    }
}
