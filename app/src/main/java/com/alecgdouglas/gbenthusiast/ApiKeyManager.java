package com.alecgdouglas.gbenthusiast;

import android.content.Context;
import android.os.AsyncTask;
import android.security.KeyPairGeneratorSpec;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.x500.X500Principal;

public class ApiKeyManager {
    private static final String TAG = "ApiKeyManager";

    private static final String KEY_STORE_TYPE = "AndroidKeyStore";
    private static final String FILE_NAME = "user_api_key";
    private static final String API_KEY_ALIAS = "api_key";
    private static final String ENCODING = StandardCharsets.UTF_8.name();
    private static final String CIPHER_TRANSFORMATION = "RSA/ECB/PKCS1Padding";
    private static final String KEY_ALGORITHM_RSA = "RSA";
    private static String sApiKey;

    private ApiKeyManager() {
    }

    private static KeyStore initializeKeyStore(Context context) {
        final KeyStore keyStore = loadKeyStore();
        if (keyStore == null) return null;

        try {
            if (!keyStore.containsAlias(API_KEY_ALIAS)) {
                Log.w(TAG, "Key store doesn't contain our API key alias, generating keys");
                generateKeys(context);
            } else {
                Log.w(TAG, "Key store contains our api key alias");
            }
        } catch (KeyStoreException e) {
            Log.w(TAG, "KeyStoreException hit while checking if it contains an alias", e);
            return null;
        }
        return keyStore;
    }

    /**
     * @return A loaded KeyStore or null if loading was not successful.
     */
    private static KeyStore loadKeyStore() {
        KeyStore keyStore;
        try {
            keyStore = KeyStore.getInstance(KEY_STORE_TYPE);
            if (keyStore == null) return null;
        } catch (KeyStoreException e) {
            Log.w(TAG, "KeyStoreException thrown while getting the KeyStore instance", e);
            return null;
        }

        try {
            keyStore.load(null);
        } catch (IOException | NoSuchAlgorithmException | CertificateException e) {
            Log.w(TAG, "Exception thrown while loading the keystore", e);
            return null;
        }

        return keyStore;
    }

    private static KeyStore.PrivateKeyEntry getPrivateKeyEntry(KeyStore keyStore) {
        KeyStore.PrivateKeyEntry privateKeyEntry;
        try {
            privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(API_KEY_ALIAS, null);

            if (privateKeyEntry == null) {
                Log.w(TAG, "Private key entry is null");
                return null;
            }
        } catch (NoSuchAlgorithmException | UnrecoverableEntryException | KeyStoreException e) {
            Log.w(TAG, "Exception hit while retrieving private key entry", e);
            return null;
        }

        return privateKeyEntry;
    }

    private static Cipher getOutCipher(PrivateKey privateKey) {
        Cipher outCipher;
        try {
            outCipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
            outCipher.init(Cipher.DECRYPT_MODE, privateKey);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            Log.w(TAG, "Exception hit while creating out-cipher", e);
            return null;
        }
        return outCipher;
    }

    private static Cipher getInCipher(PublicKey publicKey) {
        Cipher inCipher;
        try {
            inCipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
            inCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            Log.w(TAG, "Exception hit while creating in-cipher", e);
            return null;
        }
        return inCipher;
    }

    public static String getApiKey(Context context) {
        if (sApiKey != null) {
            return sApiKey;
        }

        if (context == null) return null;

        final File dataFile = getEncryptedDataFile(context);
        if (!dataFile.exists()) {
            Log.d(TAG, "API key file does not exist - API key is not stored");
            return null;
        }

        final KeyStore keyStore = initializeKeyStore(context);
        if (keyStore == null) return null;

        KeyStore.PrivateKeyEntry privateKeyEntry = getPrivateKeyEntry(keyStore);
        if (privateKeyEntry == null) return null;

        Cipher outCipher = getOutCipher(privateKeyEntry.getPrivateKey());
        if (outCipher == null) return null;

        try (final FileInputStream fis = new FileInputStream(dataFile);
             final CipherInputStream cis = new CipherInputStream(fis, outCipher);
             final ByteArrayOutputStream baos = new ByteArrayOutputStream(40)) {
            int nextByte;
            while ((nextByte = cis.read()) != -1) {
                baos.write((byte) nextByte);
            }

            Log.d(TAG, "Read " + baos.size() + " bytes from encrypted API key file");
            final String apiKey = new String(baos.toByteArray(), 0, baos.size(), ENCODING);
            if (apiKey.isEmpty()) {
                Log.d(TAG, "Read API key is null or empty");
                return null;
            }
            sApiKey = apiKey;
            return apiKey;
        } catch (IOException e) {
            Log.w(TAG, "Exception hit while reading file: " + dataFile.getAbsolutePath(), e);
        }

        return null;
    }

    private static File getEncryptedDataFile(Context context) {
        final File filesDirectory = new File(context.getFilesDir().getAbsolutePath());
        return new File(filesDirectory, FILE_NAME);
    }

    private static void generateKeys(Context context) {
        final Calendar notBefore = Calendar.getInstance();
        final Calendar notAfter = Calendar.getInstance();
        notAfter.add(Calendar.YEAR, 1);

        try {
            final KeyPairGenerator generator =
                    KeyPairGenerator.getInstance(KEY_ALGORITHM_RSA, KEY_STORE_TYPE);
            generator.initialize(new KeyPairGeneratorSpec.Builder(context).setAlias(API_KEY_ALIAS)
                    .setKeyType(KEY_ALGORITHM_RSA).setKeySize(2048).setSubject(new X500Principal(
                            String.format("CN=%s OU=%s", API_KEY_ALIAS, context.getPackageName())))
                    .setSerialNumber(BigInteger.ONE).setStartDate(notBefore.getTime())
                    .setEndDate(notAfter.getTime()).build());
            generator.generateKeyPair();
        } catch (NoSuchProviderException | NoSuchAlgorithmException |
                InvalidAlgorithmParameterException e) {
            Log.w(TAG, "Exception hit during key pair generation", e);
        }
    }

    private static boolean storeApiKeySync(Context context, String apiKey) {
        final KeyStore keyStore = initializeKeyStore(context);
        if (keyStore == null) return false;

        KeyStore.PrivateKeyEntry privateKeyEntry = getPrivateKeyEntry(keyStore);
        if (privateKeyEntry == null) return false;

        final Cipher intCipher = getInCipher(privateKeyEntry.getCertificate().getPublicKey());

        final File dataFile = getEncryptedDataFile(context);
        if (dataFile.exists()) {
            final boolean deleted = dataFile.delete();
            if (!deleted) Log.i(TAG, "Failed to delete API key file before storing new key");
        }

        try (final FileOutputStream fos = new FileOutputStream(dataFile);
             final CipherOutputStream cos = new CipherOutputStream(fos, intCipher)) {
            byte[] outBytes = apiKey.getBytes(ENCODING);
            Log.d(TAG, "Writing " + outBytes.length + " bytes to " + dataFile.getAbsolutePath());
            cos.write(outBytes);
            cos.flush();
            sApiKey = apiKey;
            return true;
        } catch (IOException e) {
            Log.w(TAG, "Exception hit while writing out to file: " + dataFile.getAbsolutePath(), e);
        }
        return false;
    }

    public static void storeApiKey(final Context context, final String apiKey,
                                   final OnStoreCompleteCallback callback) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            public Boolean doInBackground(Void... unused) {
                return storeApiKeySync(context, apiKey);
            }

            @Override
            public void onPostExecute(Boolean success) {
                callback.onStoreComplete(success);
            }
        }.execute();
    }

    public static boolean deleteApiKey(Context context) {
        sApiKey = null;
        final File dataFile = getEncryptedDataFile(context);
        return dataFile.exists() && dataFile.delete();
    }

    public static boolean hasApiKey(Context context) {
        return getApiKey(context) != null;
    }

    public interface OnStoreCompleteCallback {
        void onStoreComplete(boolean success);
    }
}
