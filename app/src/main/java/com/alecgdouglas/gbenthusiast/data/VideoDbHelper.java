/*
 * This file was copied from the Android TV Leanback sample project and modified by Alec Douglas.
 * https://github.com/googlesamples/androidtv-Leanback
 * The original file included the copyright notice below:
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alecgdouglas.gbenthusiast.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.alecgdouglas.gbenthusiast.LastRefreshTimePrefUtils;
import com.alecgdouglas.gbenthusiast.data.VideoContract.VideoEntry;
import com.alecgdouglas.gbenthusiast.data.VideoContract.VideoPosition;

/**
 * VideoDbHelper manages the creation and upgrade of the video database.
 */
public class VideoDbHelper extends SQLiteOpenHelper {
    private static final String TAG = "VideoDbHelper";
    // Change this when you change the database schema.
    private static final int DATABASE_VERSION = DbVersion.FETCH_PAGES.getVersionId();
    // The name of our database.
    private static final String DATABASE_NAME = "gbenthusiast.db";
    private final Context mContext;

    public VideoDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create a table to hold videos.
        final String SQL_CREATE_VIDEO_TABLE =
                "CREATE TABLE IF NOT EXISTS " + VideoEntry.TABLE_NAME + " (" + VideoEntry._ID +
                        " INTEGER PRIMARY KEY," + VideoEntry.COLUMN_GIANT_BOMB_ID +
                        " TEXT UNIQUE NOT NULL, " + VideoEntry.COLUMN_NAME + " " +
                        "TEXT NOT NULL, " + VideoEntry.COLUMN_DECK + " TEXT NOT NULL, " +
                        VideoEntry.COLUMN_TYPE_NAME + " TEXT NOT NULL, " +
                        VideoEntry.COLUMN_PUBLISH_DATE + " TEXT " + "" + "" + "NOT NULL, " +
                        VideoEntry.COLUMN_LENGTH_SECONDS + " INTEGER NOT NULL, " +
                        VideoEntry.COLUMN_VIDEO_URL_HD + " TEXT NOT NULL, " +
                        VideoEntry.COLUMN_VIDEO_URL_HIGH + " TEXT NOT NULL, " +
                        VideoEntry.COLUMN_VIDEO_URL_LOW + "" + " TEXT NOT NULL, " +
                        VideoEntry.COLUMN_IMAGE_URL_TINY + " TEXT NOT NULL, " +
                        VideoEntry.COLUMN_IMAGE_URL_MEDIUM + " TEXT NOT NULL, " +
                        VideoEntry.COLUMN_IMAGE_URL_SUPER + " TEXT NOT NULL, " +
                        VideoEntry.COLUMN_IMAGE_URL_ICON + " TEXT NOT NULL" + ");";

        final String SQL_CREATE_VIDEO_POSITION_TABLE =
                "CREATE TABLE IF NOT EXISTS " + VideoPosition.TABLE_NAME + " (" +
                        VideoPosition._ID + " INTEGER PRIMARY KEY," +
                        VideoPosition.COLUMN_VIDEO_ID + " INTEGER UNIQUE NOT NULL, " +
                        VideoPosition.COLUMN_VIDEO_POSITION_MILLIS + " INTEGER NOT NULL, " +
                        VideoPosition.COLUMN_LAST_UPDATED + " INTEGER NOT NULL, " + "FOREIGN KEY(" +
                        VideoPosition.COLUMN_VIDEO_ID + ") REFERENCES " + VideoEntry.TABLE_NAME +
                        "(" + VideoEntry.COLUMN_GIANT_BOMB_ID + ")" + ");";

        // Do the creating of the databases.
        Log.i(TAG, "Creating " + VideoEntry.TABLE_NAME + " table in database");
        db.execSQL(SQL_CREATE_VIDEO_TABLE);
        Log.i(TAG, "Creating " + VideoPosition.TABLE_NAME + " table in database");
        db.execSQL(SQL_CREATE_VIDEO_POSITION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);

        if (oldVersion == DbVersion.POSITION_REFERENCES_GB_ID.getVersionId() &&
                newVersion == DbVersion.POSITION_LAST_UPDATED_COLUMN.getVersionId()) {
            db.execSQL("ALTER TABLE " + VideoPosition.TABLE_NAME + " ADD COLUMN " +
                    VideoPosition.COLUMN_LAST_UPDATED + " INTEGER NOT NULL DEFAULT 0");
        } else if (newVersion == DbVersion.FETCH_PAGES.getVersionId()) {
            // Clear out the old video database, need to re-fetch everything.
            Log.i(TAG, "Clearing out just the videos database");
            db.execSQL("DROP TABLE IF EXISTS " + VideoEntry.TABLE_NAME);
            onCreate(db);
            LastRefreshTimePrefUtils.reset(mContext);
        } else {
            // Unhandled DB upgrade - simply discard all old data and start over when upgrading.
            clearDatabase();
        }
    }

    public void clearDatabase() {
        Log.i(TAG, "Clearing videos database");
        final SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + VideoEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + VideoPosition.TABLE_NAME);
        onCreate(db);

        // Going to need to repopulate the database - reset the refresh pref.
        LastRefreshTimePrefUtils.reset(mContext);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Do the same thing as upgrading...
        onUpgrade(db, oldVersion, newVersion);
    }

    private enum DbVersion {
        INITIAL(1),
        POSITION_REFERENCES_GB_ID(2),
        POSITION_LAST_UPDATED_COLUMN(3),
        FETCH_PAGES(4);

        private final int mVersionId;

        DbVersion(int versionId) {
            mVersionId = versionId;
        }

        public int getVersionId() {
            return mVersionId;
        }
    }
}
