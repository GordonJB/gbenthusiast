package com.alecgdouglas.gbenthusiast.ui;

public class DrawableGridItem {
    // Unique ID for distinguishing items for executing actions when they are clicked on.
    private final int mId;
    private final String mText;
    private final int mDrawableResourceId;

    public DrawableGridItem(int id, String text, int drawableResourceId) {
        mId = id;
        mText = text;
        mDrawableResourceId = drawableResourceId;
    }

    public int getId() {
        return mId;
    }

    public String getText() {
        return mText;
    }

    public int getDrawableResourceId() {
        return mDrawableResourceId;
    }
}

