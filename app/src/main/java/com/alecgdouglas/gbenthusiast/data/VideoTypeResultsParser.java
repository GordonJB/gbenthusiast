package com.alecgdouglas.gbenthusiast.data;

import android.util.Log;

import com.alecgdouglas.gbenthusiast.model.VideoType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VideoTypeResultsParser implements JsonResultsParser<VideoType> {
    private static final String TAG = "VideoTypeResultsParser";

    public VideoTypeResultsParser() {
    }

    @Override
    public List<VideoType> parse(JSONObject json) {
        final List<VideoType> videoTypes = new ArrayList<>();

        if (json == null) {
            Log.d(TAG, "Null JSON passed in, returning empty list");
            return videoTypes;
        }

        final JSONArray resultsArray = json.optJSONArray("results");
        if (resultsArray == null) {
            Log.d(TAG, "Unable to find results array in results JSON, returning empty list");
            return videoTypes;
        }

        for (int i = 0; i < resultsArray.length(); i++) {
            videoTypes.add(parseVideoType(resultsArray.optJSONObject(i)));
        }

        return videoTypes;
    }

    private VideoType parseVideoType(JSONObject videoTypeJson) {
        if (videoTypeJson == null) return null;
        try {
            final String name = videoTypeJson.getString("name");
            final long id = videoTypeJson.getLong("id");
            final String deck = videoTypeJson.getString("deck");
            return new VideoType(name, id, deck);
        } catch (JSONException e) {
            Log.w(TAG, "Unable to parse VideoType from JSON object", e);
        }
        return null;
    }
}
